# Windows

## Pid chas vstanovlennia Windows

na pochatku nalashtuvannia ```Ctrl+Shift+F3```, Windows perezavantazhuietsia u **audit mode** i zavantazhuie admin akaunt, pislia zavantazhennia ziavytsia **System Preparation Tools** -> **Cancel** -> vidkryvaiemo **Notepad** i kopiiuiemo

```xml
<?xml version="1.0" encoding="utf-8">
<unattend xmlns="urn:schemas-microsoft-com:wnattend">
<settings pass="oobeSystem">
<component name="Microsoft-Windows-Shell-Setup" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS" xmlns:wcm="http://chemas.microsoft.com/WMIConfig/2002/State" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instante">
<FolderLocations>
<ProfilesDirectory>D:\Users</ProfilesDirectory>
</FolderLocations>
</component>
</settings>
</unattend>
```
zberihaiemo fail pid nazvoiu *relocate.xml* na *D:*\
zapuskaiemo **Command Prompt**\
zupyniaiemo **WMP Network Sharing Service**\

```cmd
net stop wmpnetworksvc
```

teper pryminiaiemo zminy

```cmd
%windir%\system32\sysgrep\sysgrep.exe /oobe /reboot /unattend:d:\relocate.xml
```

Yakshcho vse normalno Windows perezavantazhuiemo i pochnetsia nalashtuvannia Windows\
Pislia vstanovlennia papka **Users** maie buty na dysku **D:**\

**Pislia vstanovlennia mozhna pochaty nalashtuvannia Windows**\

## Settings

|     |     |     |     |     |
|-----|-----|-----|-----|-----|
|System|     |     |     |     |
|     |Display|     |     |     |
|     |     |Night light|On|     |
|     |Storage|     |     |     |
|     |     |Storage Sense|On|     |
|     |Multitasking|     |     |     |
|     |     |Alt + Tab|Open windows only|     |
|     |Clipboard|     |     |     |
|     |     |Clipboard history|On|     |
|     |     |Sync across your devices|on|     |
|     |     |Automatically sync text I copy|     |     |
|Bluetooth & devices|     |     |     |     |
|     |Your Phone|     |     |     |
|     |     |Get instant access to your Android device's photos, texts and more|     |     |
|Personalization|     |     |     |     |
|     |Color|     |     |     |
|     |     |Show accent color on Start and taskbar|On|     |
|     |     |Show accent color on title bars and windows borders|On|     |
|Time & language|     |     |     |     |
|     |Language & region|     |     |     |
|     |     |Typing|     |     |
|     |     |     |Advanced keyboard settings|Let me yse a different input method for each app window|
|Privacy & security|     |     |     |     |
|     |Searching Windows|     |     |     |
|     |     |Find my File|Envhaned|     |
|Windows Update|     |     |     |     |
|     |Advanced Options|     |     |     |
|     |     |Delivery Optimization|     |     |
|     |     |     |Allow downloads from other PCs|Off|
